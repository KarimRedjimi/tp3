package com.example.tp3_version2;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends AppCompatActivity {


    ListView list;
    Cursor cursor;
    SimpleCursorAdapter cursorAdapter;
    WeatherDbHelper weatherDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        list = findViewById(R.id.myList);

        weatherDbHelper = new WeatherDbHelper(this);

        weatherDbHelper.populate();

        cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, weatherDbHelper.fetchAllCities () , new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper. COLUMN_COUNTRY }, new int[] { android.R.id.text1, android.R.id.text2});

        list.setAdapter(cursorAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent myIntent = new Intent(v.getContext(), CityActivity.class);

                cursor = (Cursor) list.getItemAtPosition(position);

                City citySelected = WeatherDbHelper.cursorToCity(cursor);

                Log.d("opening city activity", citySelected.toString());

                myIntent.putExtra("CitySelected", citySelected);
                startActivity(myIntent);
            }
        });



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), NewCityActivity.class);
                myIntent.putExtra("NewCitySelected", (Bundle) null);
                startActivity(myIntent);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MainActivity :: onResume()");
        cursor = weatherDbHelper.fetchAllCities();
        cursorAdapter.changeCursor(cursor);
        cursorAdapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




}

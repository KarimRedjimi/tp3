package com.example.tp3_version2;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.security.PrivateKey;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    private City city;
    private WeatherDbHelper weatherDbHelper2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);


        weatherDbHelper2 = new WeatherDbHelper(this);
        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();

        city = (City) extras.get("NewCitySelected");


        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);




        final Button but = (Button) findViewById(R.id.button);


        but.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                    //IL FAUT AMELIORER CA, C PAS LA MEILLEUR METHODE 
                    city = new City(textName.getText().toString(),textCountry.getText().toString());
                    weatherDbHelper2.addCity(city);
                    Intent intent = new Intent(NewCityActivity.this,MainActivity.class);
                    startActivity(intent);
            }
        });
    }

    private void updateView()
    {
        textName.setText(city.getName());
        textCountry.setText(city.getCountry());


    }


}
